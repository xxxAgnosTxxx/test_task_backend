# Test_task_Backend

Тестовое задание на Backend для компании Profsoft

## Перед запуском

1. Скачать и установить JDK 1.8 - [https://www.oracle.com/cis/java/technologies/javase/javase8-archive-downloads.html]
2. Установить IDE Intellij Idea - [https://www.jetbrains.com/ru-ru/idea/]
3. Запустить среду разработки и склонировать в новый проект репозиторий. Воспользуйтесь этим гайдом - [https://javarush.ru/groups/posts/2818-podruzhim-git-s-intellij-idea]
4. 1 - 6 задания запускаются в классе Application.

## Для задания 7:
1. В среде Intellj Idea нажать на "File" -> "Settings" -> "Plugins". 
2. Вбить в поиске "Smart Tomcat", сделать install, apply и restart IDE.
3. После перезагрузки на верхней панели нажать на "Edit Configuration". Нажать на "+", в списке выбрать "Smart Tomcat".
4. Во вкладке Configuration около пункта Tomcat Server нажать на кнопку Configuration. 
    Нажать на "+" и задать путь "'Путь до проекта'\test_task_backend\web\WEB_INF\classes". 
    Нажать apply и закрыть окно.
5. Во вкладке Configuration около пункта Deployment Directory изменить путь на "'Путь до проекта'\test_task_backend\web".
6. Нажать apply и в Edit Configuration выбрать и запустить настроенный Tomcat server.
7. Для отображения Web страниц после запуска перейдите по ссылке: http://localhost:8080/test_task_backend


## Дополнительно
Каждая ветка, кроме main, содержит испоняемый код. Названия веток: task(номер задания)
